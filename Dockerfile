FROM node:13-alpine

WORKDIR /app
 
COPY package*.json /app/

RUN npm install

COPY . .

EXPOSE 8080

ENV CLASS="SHB_DevOps" \
    APP_PORT=8080 \
    APP_ENV=dev

LABEL "Ho va ten"="Nguyen Hoang Huy" \
      "Email"="huynh.bk1.devopsonline23-04.2510@student.bkacad.edu.vn"

CMD ["node", "server.js"]
